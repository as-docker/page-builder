# About this image

This image is based on our [Phalcon 1.3.4](https://hub.docker.com/r/amsdard/phalcon/) image.
It is extended to provide support for basic projects Vegas CMF Page Builder is used.

### Docker-compose example

```
webapp:
    image: amsdard/page-builder:php5.5-apache
    volumes:
      - ./:var/www/html
    environment:
      - TERM=xterm-256color
```

This will mount your application under */var/www/html* directory and run apache web server for your app. The container *webapp* exposes port 80 by default.

## Customization

Additional project requirements will expect the image to be extended.

### Project Dockerfile example

```
FROM amsdard/page-builder:php5.5-apache
MAINTAINER Vegas CMF <vegas@amsterdam-standard.pl>

# Use when a debugger is needed
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

# Use when MySQL database support is needed
RUN docker-php-ext-install pdo_mysql

# Install a couple of locales for i18n support in project
RUN ${apt_update} && ${apt_install} locales \
    && locale-gen nl_NL \
    && locale-gen nl_NL.UTF8 \
    && locale-gen pl_PL \
    && locale-gen pl_PL.UTF8 \
    && locale-gen \
    && ${apt_clean}
```